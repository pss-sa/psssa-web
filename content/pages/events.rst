Events run with the assistance of the PSS-SA
============================================

This lists events run with the assistance of the PSS-SA, and includes
links to any notes from the event.

* `PyCon ZA 2018`_
* `PyCon ZA 2019`_
* `PyCon ZA 2020`_
  - Experiences from using `Big Blue Button`_ for an online conference.
* `PyCon ZA 2021`_
* `PyCon ZA 2022`_
* `PyCon ZA 2023`_

Return to the `Home Page`_.

.. _PyCon ZA 2018: https://2018.za.pycon.org/
.. _PyCon ZA 2019: https://2019.za.pycon.org/
.. _PyCon ZA 2020: https://2020.za.pycon.org/
.. _Big Blue Button: {filename}pycon_za_2020_bbb.rst
.. _PyCon ZA 2021: https://2021.za.pycon.org/
.. _PyCon ZA 2022: https://2022.za.pycon.org/
.. _PyCon ZA 2023: https://2023.za.pycon.org/
.. _Home Page: {filename}index.rst
