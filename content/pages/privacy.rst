Web site privacy policy
=======================

Like most web sites, we collect and log various bits of information from visitors
to the web site.

*Information collected*

We collect and log the following:

* IP address

* Time and date of the request

* Specific page requested

* Browser identification string (which may include details of the operating system and browser version)

* The referral URL.

* HTTP status codes for the request.

*How we use the infomation*

The log file data is used to monitor the performance of the web site and for
detecting attempts at abuse.

*How long is the data retained*

Log files are deleted within 1 calendar month of being collected.
