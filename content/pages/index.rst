The Python Software Society of South Africa
===========================================

Welcome to the web page of the Python Software Society of South Africa, a
non-profit corporation dedicated to promoting the Python_ programming
language.

More information coming soon, but you can see Simon Cross' introduction
to the PSSSA given at `PyConZA 2017`_.

See the list of `Events`_ run with the assistance of the PSS-SA.

See our web site `Privacy Policy_`.


.. _Python: http://python.org/
.. _PyconZA 2017: https://youtu.be/BlcnfzpapmU
.. _Events: {filename}events.rst
.. _Privacy Policy: {filename}privacy.rst
